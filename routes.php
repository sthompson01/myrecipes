<?php

namespace App\Controllers;

$page = isset($_GET['page']) ? $_GET['page'] : 'home';

switch ($page) {
	case 'home':
		$controller = new HomeController();
		$controller->show();
		break;

	case 'personalmessage':
		$controller = new ContactController();
		$controller->show();
		break;

	case 'category':
		$controller = new RecipesController();
		$controller->showitems();
		break;

	case 'recipes':
		$controller = new RecipesController();
		$controller->show();
		break;

	case 'recipe.edit':
		$controller = new RecipesController();
		$controller->edit();
		
	case 'singlerecipe':
		$controller = new RecipesController();
		$controller->showIndividualRecipe();
		break;

	case 'register':
		$controller = new AuthenticationController();
		$controller->register();
		break;

	case 'auth.store':	
		$controller = new AuthenticationController();
		$controller->store();
		break;

	case 'auth.attempt':	
		$controller = new AuthenticationController();
		$controller->attempt();

		break;

	case 'login':
		$controller = new AuthenticationController();
		$controller->login();
		break;

	// case 'account':
	// 	$controller = new AuthenticationController();
	// 	$controller->show();
	// 	break;

	case 'edit':
		$controller = new AuthenticationController();
		$controller->edit();

	case 'logout':
		$controller = new AuthenticationController();
		$controller->logout();
		break;

	
	default:
		echo "404";
		break;
}