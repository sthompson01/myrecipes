<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Everyday Chef</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'> -->
</head>
<body>
	<nav class="navbar navbar-default">
  	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><!-- <img src="images/everydaychef.png"> -->Everyday Chef</a>
    </div>

      <form class="navbar-form navbar-left" role="search" id="search-bar">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
      </form>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li<?php if ($page === "recipes"): ?> <?php endif ;?>><a href=".\?page=category">Recipes</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
      <?php if(! static::$auth->check()): ?>
        <li<?php if($page === "register"): ?> class="active" <?php endif ;?>><a href=".\?page=register">Sign Up</a></li>
        <li<?php if($page === "login"): ?> class="active" <?php endif ;?>><a href=".\?page=login">Log In</a></li>
        <?php else: ?>
            <li><a href=".\?page=edit&amp;id=<?= static::$auth->user()->user_id; ?>"><?= static::$auth->user()->email; ?></a></li> 
            <li><a href=".\?page=logout">Logout</a></li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>

   <?php $this->content(); ?>

   <footer>
     <div id="contact-details">
          <p>We are based out of Wellington, New Zealand.</p><br>
          <p>Contact us on:</p>
          <p>0800 RECIPESNZ</p>
          <p>myrecipes@mail.com</p>
      </div>

        <div class="message-space">
          <form id="contact-form" action=".\?page=personalmessage" method="POST" class="form-horizontal">
            <h4>Send us a direct message:</h4><br>
              <div class="form-group <?php if($message['errors']['email']): ?> has-error <?php endif; ?>">
                <label for="emailaddress" class="col-sm-2 control-label">Email Address</label>
                <div class="col-sm-4">
                  <input type="email" class="form-control  <?php if($message['errors']['email']): ?> has-error <?php endif; ?>" id="email" name="email" value="<?php echo $message['email']; ?>">
                  <div class="help-block"><?php echo $message['errors']['email']; ?></div>
              </div>
            </div>

            <div class="form-group <?php if($message['errors']['message']): ?> has-error <?php endif; ?>">
              <label for="message" class="col-sm-2 control-label">Message</label>
              <div class="col-sm-4">
                <textarea class="form-control" id="message" name="message" value="<?php echo $message['message']; ?>"></textarea>
                <span class="help-block"><?php echo $message['errors']['message']; ?></span>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-4">
                  <button type="submit" class="btn btn-default">Send</button>
              </div>
            </div>
        </form>
       </div>
      </div>

        <div class="social-media-icons">
              <a href="https://www.facebook.com"><img src="images/facebook-icon.png"></a>
              <a href="https://www.instagram.com"><img src="images/instagram-icon.png"></a>
              <a href="https://www.twitter.com"><img src="images/twitter-icon.png"></a>
              <a href="https://www.pinterest.com"><img src="images/pinterest-icon.png"></a>
            </div>
          </div>
        </div>

   <!--  <div clas="row" id="copyright">
      <div class="col-md-2"> -->
        <!-- <img src="images/everydaychef-white.png" id="everydaychef"> -->
        <!-- <p>&#169 Everyday Chef, 2016</p>
        <p>Sarah Thompson Web Design & Development</p>
      </div>
    </div> -->

        <div class="quick-links">
          <li<?php if($page === "recipes"): ?> class="active" <?php endif ;?>><a href=".\?page=recipes">Recipes</a></li>
          <li<?php if($page === "register"): ?> class="active" <?php endif ;?>><a href=".\?page=register">Sign Up</a></li>
          <li<?php if($page === "login"): ?> class="active" <?php endif ;?>><a href=".\?page=login">Log In</a></li>  
        </div>
        </div>
      </div>

  </footer>



  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>



</body>
</html>