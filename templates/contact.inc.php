<?php 
$title = "Contact";
$page = "contact";
include "master.inc.php";

function content() {

?>

<h2 class="user-action">Send us a message</h2>

	<form class="form-horizontal">
  	<div class="form-group">
    	<label for="emailaddress" class="col-sm-2 control-label">Email Address</label>
    	<div class="col-sm-4">
      	<input type="email" class="form-control" id="email">
    </div>
    </div>

    <div class="form-group">
    	<label for="message" class="col-sm-2 control-label">Your message</label>
    	<div class="col-sm-4">
        <textarea class="form-control" id="message" name="description"></textarea>
        <span class="help-block"></span>
    </div>
    </div>

    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
      <button type="submit" class="btn btn-default">Send</button>
    	</div>
  	</div>
	</form>


<?php
}