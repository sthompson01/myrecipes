<div id="recipes">
		<h2>Recipe Categories</h2>
	   <div class="categories">
		   <a href=".\?page=recipes&amp;category=salads"><img src="images/salads.jpg" alt="salads-category" class="img-rounded" class="img-responsive"></a>
		   <a href=".\?page=recipes&amp;category=pasta"><img src="images/pasta.jpg" alt="pasta-category" class="img-rounded" class="img-responsive"></a>
		  <a href=".\?page=recipes&amp;category=dessert"><img src="images/dessert.jpg" alt="dessert-category" class="img-rounded" class="img-responsive"></a>
		</div>
		<div class="categories">
		   <a href=".\?page=recipes&amp;category=baking"><img src="images/baking.jpg" alt="baking-category" class="img-rounded" class="img-responsive"></a>
		   <a href=".\?page=recipes&amp;category=appetizers"><img src="images/appetizers.jpg" alt="appetizers-category" class="img-rounded" class="img-responsive"></a>
		   <a href=".\?page=recipes&amp;category=vegetarian"><img src="images/vegetarian.jpg" alt="vegetarian-category" class="img-rounded" class="img-responsive"></a>
	   </div>
   </div>
