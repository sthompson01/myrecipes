		
<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
		  <li><a href=".\?page=category">Recipe Categories</a></li>
		  <li class="active"><?=$category;?></li>
		</ol>

		<?php if(count($recipes) > 0): ?>
			<ol>
				<?php foreach ($recipes as $recipe) :?>
					<li>
						<h3>
							<a href="./?page=singlerecipe&amp;id=<?= $recipe->recipe_id; ?>">
							<?= $recipe->recipe_name; ?> </a>
						</h3>
						
					</li>
				<?php endforeach; ?>
			</ol>
		<?php else: ?>
			<p>Weirdly enough, there are no recipes to display. Spooky!!! </p>
		<?php endif; ?>	

	</div>
</div>