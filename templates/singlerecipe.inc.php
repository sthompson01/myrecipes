<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
		  <li><a href=".\?page=category">Recipe Categories</a></li>
		  <!-- <li class="active"><?=$recipes_listrecipe_category; ?></li> -->
		  <li><a href=".\?page=recipes">Recipe Category List</a></li>
		  <li class="active"><?= $recipe->recipe_name; ?></li>
		</ol>
			
		<h2><?= $recipe->recipe_name; ?></h2>

		<p id="single-recipe"><?= $recipe->recipe_details; ?></p>