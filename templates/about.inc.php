<?php 
$title = "About";
$page = "about";
include "master.inc.php";

function content() {

?>

<div id="onion-image">
		<img src="images/onions-slogan.jpg">
	</div>

	<div class="container">	
	<div class="row">
	<p id="favourites"><i>Check out some of our favourites:</i></p>
  			<div class="col-md-3">
  				<figure>
					<a href="#"><img src="images/cajun-chicken-pasta.png"></a>
					<figcaption>Creamy Cajun Chicken Pasta</figcaption>
				</figure>
			</div>

		<div class="col-md-3">
			<figure>
				<a href="#"><img src="images/broccoli-salad.png"></a>
				<figcaption>Broccoli Salad</figcaption>
			</figure>
		</div>

		<div class="col-md-3">
			<figure>
				<a href="#"><img src="images/brownie.png"></a>
				<figcaption>The World's Best Brownie</figcaption>
			</figure>
		</div>

		<div class="col-md-3">
			<figure>
				<a href="#"><img src="images/asparagus.png"></a>
				<figcaption>Asparagus with Proscuitto</figcaption>
			</figure>
		</div>
	</div>
	</div>


<?php 
}
