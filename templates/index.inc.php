<div id="onion-image">
		<img src="images/onions.jpg">
	</div>

	<!-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> -->
  <!-- Indicators -->
  <!-- <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
 -->
  <!-- Wrapper for slides -->
 <!--  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="images/onions.jpg" alt="onions-image">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    <div class="item">
      <img src="images/steak.jpg" alt="">
      <div class="carousel-caption">
      </div>
    </div>
     <div class="item">
      <img src="images/steak.jpg" alt="">
      <div class="carousel-caption">
      </div>
    </div>
  </div>
 -->
  <!-- Controls -->
 <!--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> -->

	<div class="container">	
	<div class="row">
	<p id="favourites">Check out some of our favourites recipes</p>
  			<div class="col-md-3">
  				<figure>
					<a href=".\?page=recipes"><img src="images/cajun-chicken-pasta.png"></a>
					<figcaption>Creamy Cajun Chicken Pasta</figcaption>
				</figure>
			</div>

		<div class="col-md-3">
			<figure>
				<a href=".\?page=recipes"><img src="images/broccoli-salad.png"></a>
				<figcaption>Broccoli Salad</figcaption>
			</figure>
		</div>

		<div class="col-md-3">
			<figure>
				<a href=".\?page=recipes"><img src="images/brownie.png"></a>
				<figcaption>The World's Best Brownie</figcaption>
			</figure>
		</div>

		<div class="col-md-3">
			<figure>
				<a href=".\?page=recipes"><img src="images/asparagus.png"></a>
				<figcaption>Asparagus with Proscuitto</figcaption>
			</figure>
		</div>
	</div>
	</div>


