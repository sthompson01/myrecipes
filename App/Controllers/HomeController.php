<?php 

namespace App\Controllers;

use App\Views\HomeView;
use App\Views\View;


class HomeController extends Controller
{
		public function show()
	{
		$message = $this->getMessageFormData();
		
		$view = new HomeView(compact('message'));
		$view->render();
	}
	public function getMessageFormData()
	{
		if(isset($_SESSION['message'])){
			$message = $_SESSION['message'];
		} else {
			$message = [
				'email' => "",
				'message' => "",
				'errors' => [
					'email' =>"",
					'message' =>""
				]
			];
		}
		return $message;
	}

}
