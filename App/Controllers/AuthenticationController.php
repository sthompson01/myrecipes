<?php

namespace App\Controllers;

use App\Views\RegisterFormView;
use App\Views\LoginFormView;
use App\Views\AccountEditView;
use App\Models\User;


class AuthenticationController extends Controller
{

	public function register()
	{
		$user = $this->getUserFormData();
		$view = new RegisterFormView(compact('user'));
		$view->render();
	}

	public function login()
	{
		$user = $this->getUserFormData();
		$error = isset($_GET['error']) ? $_GET['error'] : null;
		$view = new LoginFormView(compact('user', 'error'));
 		$view->render();

	}

	protected function getUserFormData($id = null)
	{
		
		if(isset($_SESSION['user.form'])){
			$user = $_SESSION['user.form'];
			unset($_SESSION['user.form']);
		} else {
			$user =new User(); 

		}
	
		return $user;
	}

	public function store()
 	{

 		$user = new User($_POST);
 		if(! $user->isValid()){
 			$_SESSION['user.form'] = $user;
 			header("Location: .\?page=register");
 			exit();
 		}
 		$user->save();
		header("Location:.\?page=login");
	}

	public function attempt()
	{
		if(static::$auth->attempt($_POST['email'],$_POST['password'])){

			// Login is successful
			header("Location:./?page=home");
			exit();

			}

		 header("Location: .\?page=login&error=true");
		 exit();
	}

	public function logout()
	{
		static::$auth->logout();
		header("Location:.\?page=login");
		exit();
	}

	public function edit()
     {

         $edituser = static::$auth->user();
         $edituser = $this->getUserFormData($_GET['id']);       

         $view = new AccountEditView(compact('edituser'));
         $view->render();
     }

     public function update()
     {
        $input = $_POST;
      
        $input['id'] = static::$auth->user()->id;

        $user = new User($input);
            
        if (! $user->isValid()) {
            $_SESSION['user.form'] = $user;
            header("Location: .\?page=account.edit&id=" .$user->id);
            exit();
            }

        $user->save();
         die();
         header("Location: .\?page=account");
        }
}

