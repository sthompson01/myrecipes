<?php

namespace App\Controllers;

use App\Views\MessageSuccessView;
use App\Views\SuggesterEmailView;
use App\Views\HostEmailView;


class ContactController extends Controller
{
	private $message = [];
	
	public function __construct()
	{
		$this->message = [
						'errors' => []
						];
	}

	public function resetSessionData()
	{
		// $_SESSION['messageerror'] = NULL;
		$_SESSION['message'] = NULL;
		
	}

	public function getFormData() 
	{
		$expectedVariables = ['email', 'message'];

		foreach ($expectedVariables as $variable) {

			// assume no errors
			$this->message['errors'][$variable]= "";

			if(isset($_POST[$variable])){
				$this->message[$variable] = $_POST[$variable];
			} else {
				$this->message[$variable] = "";
			}
		}

	}
	public function isFormValid()
	{
		//validating form
		$valid = true;


		if(!filter_var($this->message['email'], FILTER_VALIDATE_EMAIL)){
			$this->message['errors']['email']="Enter a valid email address";
			$valid = false;
		}

		if(strlen($this->message['message']) == 0) {
			$this->message['errors']['message']= "Please enter a message";
			$valid = false;
		}
		return $valid;
	}

	public function show()
	{
		$this->resetSessionData();

		//capture suggester data
		$this->getFormData();

		//validate form data
		if(! $this->isFormValid()){
			$_SESSION['message'] = $this->message;
			header("Location:.\?page=messagesuccess");
			return;
		}
		//once  form is validated, get to thanks page
		$view = new MessageSuccessView();
		$view->render();

		//send email to the suggester
		$suggesterEmail = new SuggesterEmailView($this->message);
		$suggesterEmail->render();

		//send email to the host
		$hostEmail = new HostEmailView($this->message);
		$hostEmail->render();


	}
}
