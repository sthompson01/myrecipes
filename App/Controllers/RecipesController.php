<?php 

namespace App\Controllers;

use App\Models\Recipes;
use App\Views\RecipesView;
use App\Views\RecipesListView;
use App\Views\SingleRecipeView;
use App\Views\RecipeCreateView;


class RecipesController extends Controller
{
	public function showitems()
	{
		
		$view = new RecipesView();
		$view->render();
		
	}

		public function show()
	{
		$category = $_GET['category'];

		$recipes = Recipes::all("recipe_name", true, null, null, $category);
		
		$view = new RecipesListView(compact('recipes', 'category'));
		$view->render();

	}

	public function showIndividualRecipe()
	{

		$recipe = new Recipes((int)$_GET['id']);

		// $newcomment = $this->getCommentFormData();
		// $comments = $recipe->comments();
		// $tags = $recipe->getTags();

		$view = new SingleRecipeView(compact('recipe', 'newcomment', 'comments','tags'));
		$view->render();
	}

	// public function edit()
	// {
	// 	static::$auth->mustBeAdmin();

	// 	$movie = $this->getFormData($_GET['id']);
	// 	$movie->loadTags();

	// 	$view = new RecipeCreateView(compact('recipe','tags'));
	// 	$view->render();
	// }



}
