<?php

namespace App\Views;

class AccountEditView extends TemplateView
{
	public function render() 
	{
		extract($this->data);
		$page = "edit";
		$page_title = "User Account";
		include "templates/master.inc.php";
	}

	protected function content() {

		extract($this->data);
		include "templates/account.inc.php";
	}
}
