<?php

namespace App\Views;

class MessageSuccessView extends TemplateView
{

  public function render()
  {
      extract($this->data);
      $page = "messagesuccess";
      $page_title = "Thanks for your message!";
      include "templates/master.inc.php";
  }

  protected function content()
  {
      extract($this->data);
      include "templates/messagesuccess.inc.php";
  }
  
}
