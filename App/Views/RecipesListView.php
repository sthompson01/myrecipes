<?php 

namespace App\Views;

class RecipesListView extends TemplateView
{
	public function render() 
	{
		
		extract($this->data);
		$page = "recipes-list";
		$page_title = "Recipe Categories List";
		include "templates/master.inc.php";
	}

	protected function content() {

		extract($this->data);
		include "templates/categories-list.inc.php";
	}

}