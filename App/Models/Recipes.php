<?php

namespace App\Models;
// use Intervention\Image\ImageManagerStatic as Image;

class Recipes extends DatabaseModel {

	protected static $tableName = 'recipes';
	protected static $columns = ['recipe_id', 'recipe_name', 'recipe_details', 'recipe_category', 'poster'];
	// protected static $fakeColumns = ['tags'];
	// protected static $validationRules = [
	// 				"title"			=> "minlength:1",
	// 				"year"			=> "minlength:4,maxlength:4,numeric",
	// 				"description"	=> "minlength:10"
	// ];

}