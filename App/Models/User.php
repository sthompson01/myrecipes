<?php

namespace App\Models;

class User extends DatabaseModel
{
	protected static $tableName = "users";
	protected static $columns = ['user_id','email','username','password','role'];
	protected static $fakeColumns = ['password2'];
	protected static $validationRules = [
					"email" 		=> "email,unique:App\Models\User",
					"username"      => "minlength:3",
					"password" 		=> "minlength:6",
					"password2" 	=> "match:password"

	];
	
	public function __construct($input = null)
	{
		parent::__construct($input);
		if($this->role === null){
			$this->role = 'admin';
		}
	}

}