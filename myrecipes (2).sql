-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 12:08 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myrecipes`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categories_id` int(11) NOT NULL,
  `category_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categories_id`, `category_name`) VALUES
(1, 'Salads'),
(2, 'Pasta'),
(3, 'Dessert'),
(4, 'Baking'),
(5, 'Appetizers'),
(6, 'Vegetarian');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `recipe_id` mediumint(8) NOT NULL,
  `user_id` int(15) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `time_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `recipe_id` mediumint(8) NOT NULL,
  `recipe_name` text NOT NULL,
  `recipe_details` text NOT NULL,
  `recipe_category` text NOT NULL,
  `poster` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`recipe_id`, `recipe_name`, `recipe_details`, `recipe_category`, `poster`) VALUES
(1, 'The World''s Best Brownie', 'Servings 16\r\n\r\nIngredients:  \r\n1⁄2 cup butter, melted\r\n1⁄2 cup unsweetened cocoa\r\n1 cup sugar\r\n2 eggs\r\n2 teaspoons vanilla\r\n1⁄2 cup flour\r\n1⁄4 teaspoon salt \r\n Ingredient Additions:\r\n1 -2 cup chocolate chips (semisweet, white, butterscotch, peanut butter)\r\n1 -2 cup raisins\r\n1 -2 cup chopped maraschino cherry\r\n1 -2 cup chopped nuts\r\n1 -2 cup M&M''\r\n1 -2 cup Reese''s pieces\r\n1 -2 cup miniature marshmallow \r\n Directions:\r\nPreheat oven to 350°F.\r\nGrease an 8 inch square pan or line with foil.\r\nIn a medium bowl combine melted butter and cocoa and stir until cocoa is dissolved.\r\nAdd sugar and mix well.\r\nAdd eggs one at a time and stir until well combined.\r\nStir in vanilla, flour and salt until you no longer see any flour (do not over mix).\r\nFold in "WHATEVER FLOATS YOUR BOAT"!\r\nSpread in pan and bake for approximately 25 minutes.\r\nDO NOT OVER-BAKE -- your brownies will come out dry. Adjust time/temp accordingly for your oven. If you do the knife/toothpick test, it should come out with moist crumbs, not clean.\r\nCool completely before cutting into squares.\r\nFor vegetarian omit the marshmallows.\r\nFor double recipe, bake in 9x12 pan and add 5 minutes to baking time.\r\n', 'Dessert', NULL),
(2, 'Creamy Chicken Cajun Pasta', 'Servings 2\r\n\r\nIngredients:\r\n2 boneless skinless chicken breast halves, cut into thin strips\r\n4 ounces linguine, cooked al dente\r\n2 teaspoons cajun seasoning (your recipe, Cajun Seasoning Mix or store-bought)\r\n2 tablespoons butter\r\n1 thinly sliced green onion\r\n1 -2 cup heavy whipping cream\r\n2 tablespoons chopped sun-dried tomatoes\r\n1⁄4 teaspoon salt\r\n1⁄4 teaspoon dried basil\r\n1⁄8 teaspoon ground black pepper\r\n1⁄8 teaspoon garlic powder\r\n\r\nDirections:\r\nPlace chicken and Cajun seasoning in a bowl and toss to coat.\r\nIn a large skillet over medium heat, sauté chicken in butter or margarine until chicken is tender, about 5 to 7 minutes.\r\nReduce heat add green onion, heavy cream, tomatoes, basil, salt, garlic powder, black pepper and heat through.\r\nPour over hot linguine and toss with Parmesan cheese.\r\n\r\nGarnish:\r\n1⁄4 cup grated parmesan cheese\r\n', 'Pasta', NULL),
(3, 'Broccoli Salad', 'Servings 8-12\r\n\r\nIngredients:\r\n1 -2 head fresh broccoli (do not use frozen!)\r\n1⁄2 cup red onion, chopped\r\n1⁄2 lb bacon\r\n2 1⁄2 tablespoons vinegar\r\n1 cup Hellmann''s mayonnaise\r\n1⁄3 cup sugar\r\n1 1⁄2 cups grated mozzarella cheese\r\n\r\nDirections:\r\nCook bacon (I make it easy and buy the already cooked bacon in the refrigerator section) and crumble into pieces.\r\nChop up top of broccoli into bite size pieces.\r\nMix broccoli, onions, bacon and mozzarella in large bowl.\r\nIn separate bowl combine vinegar, sugar and mayo.\r\nPour over broccoli mixture and toss to coat.\r\nBest if made a day ahead.', 'Salads', NULL),
(4, 'Asparagus with Prosciutto ', 'Servings 16\r\n\r\nIngredients:\r\nIngredients:\r\n8 long thin slices prosciutto\r\n1 bunch asparagus, trimmed (you''ll need 16 spears)\r\n2 sheets frozen puff pastry, thawed\r\n1 egg, beaten\r\n\r\nDirections:\r\nPre heat your oven to 180c.\r\nCut the prosciutto in half lengthways, and wrap one piece of prosciutto around an asparagus spear, leaving the tip exposed.\r\nCut the pastry into 4, lay the asparagus and prosciutto across the 1/4 pastry on the diagonal, then pull it up to form an open ended parcel.\r\nLightly brush with the beaten egg.\r\nRepeat with the remaining 15.\r\nArrange on a baking tray and cook for 15 minutes or until puffed and golden.\r\nServe with a sweet chilli sauce on the side.\r\n', 'Appetizers', NULL),
(5, 'Carrot Cake', 'Servings 12\r\n\r\nIngredients:\r\nCake:\r\n2cups all-purpose flour\r\n2teaspoons baking soda\r\n1teaspoon baking powder\r\n1teaspoon salt\r\n2teaspoons ground cinnamon\r\n1 3⁄4cups white sugar\r\n1cup vegetable oil\r\n3eggs\r\n1teaspoon vanilla extract\r\n2cups shredded carrots\r\n1cup flaked coconut\r\n1cup chopped walnuts\r\n1(8 ounce) can crushed pineapple, drained\r\n\r\nFrosting:\r\n1(8 ounce) package cream cheese, softened\r\n1⁄4cup butter, softened\r\n2cups icing sugar\r\n\r\nDirections:\r\nPreheat oven to 350 degrees F (175 degrees C). Grease and flour a 9x13 inch pan.\r\nMix flour, baking soda, baking powder, salt and cinnamon; make a well in the center and add sugar, oil, eggs and vanilla. Mix with wooden spoon until smooth.\r\nStir in carrots, coconut, walnuts and pineapple. Pour into 9x13 inch pan and bake for about 45 minutes. Don''t panic, the centre will sink a little.\r\nAllow to cool; when cool, ice the cake. You can certainly use your favourite cream cheese frosting to ice this cake, but the one I''ve included is highly recommended (I usually leave the cake in the 13x9 pan and just ice the top).\r\nTo make the frosting: Cream the butter and cream cheese until smooth; add the icing sugar and beat until creamy.', 'Baking', NULL),
(6, 'Cypriot-style Potato Salad', 'Serves 6\r\n\r\nIngredients:\r\n1.2 kg Cyprus potato , or other waxy potato such as Jersey Royal or Charlotte\r\nsea salt\r\nfreshly ground black pepper\r\n1 lemon , juice of\r\nextra virgin olive oil\r\nolive oil\r\n50 g Greek olives , stones in\r\n2 tablespoons capers , rinsed\r\n1 bunch of fresh coriander\r\n6 spring onions\r\n\r\nDirections:\r\nScrub the potatoes clean, then cook in a large pan of boiling salted water for 20 to 25 minutes, or until tender but still holding their shape. Drain and set aside until just cool enough to handle, then slice into rough chunks, flaking away the skin as you go. \r\n\r\nPlace into a large bowl with the lemon juice and a good lug of extra virgin olive oil. Season lightly, then toss well so the potatoes absorb all those lovely flavours. Season to taste, then leave to cool completely.\r\nHeat 1 tablespoon of olive oil in a small frying pan over a medium heat. Crush the olives with the flat side of your knife, tear out and discard the stones, then roughly chop the flesh. Add the olives and capers to the pan and fry for 2 to 3 minutes, or until crisp. Meanwhile, pick and roughly chop the coriander leaves, then trim and finely slice the spring onions. Stir them through the salad, sprinkle over the olives and capers, then serve.', 'Salads', NULL),
(7, 'Wild Rice and Brussel Sprout Super Salad', 'Serves 8\r\n\r\nIngredients:\r\n300 g mixed wild rice\r\n2 red onions\r\n2 tablespoons red wine vinegar\r\n500 g Brussels sprouts\r\n8 radishes\r\n1 lemon\r\n1 large bunch of mixed soft fresh herbs, such as mint, parsley, basil\r\n1 large handful of dried cranberries or raisins\r\nextra virgin olive oil\r\n\r\nDirections:\r\nCook the wild rice according to the packet instructions, then drain and leave to cool on a large tray.\r\n\r\nPeel the onions, then using a mandolin (or a food processor with a fine slicing attachment), finely slice them. Transfer to a bowl and add the red wine vinegar. Scrunch them together and set aside.\r\n\r\nUsing a mandolin, shred the sprouts and radishes, then, in a bowl, dress them with the lemon juice and a pinch of sea salt, massaging the flavours into the veg with your hands. Set aside.\r\n\r\nPick and finely chop the herb leaves and pop them into a large serving bowl. Add the rice, onions and lemony veg as well as the cranberries or raisins, and toss together. \r\n\r\nDrizzle over some oil and season, then stir and serve.', 'Salads', NULL),
(8, 'Chai Sticky Date Pudding', 'Serves 10-12\r\n\r\nIngredients:\r\n150 g unsalted butter , (at room temperature), plus extra for greasing\r\n500 g self-raising flour , plus extra for dusting\r\n500 g dried pitted dates\r\n1 orange\r\n2 chai tea bags\r\n20 g bicarbonate of soda\r\n350 g dark brown sugar\r\n5 large free-range eggs\r\n1 teaspoon vanilla extract\r\n50 g golden syrup\r\n75 g black treacle\r\n\r\nToffee Sauce\r\n125 g double cream\r\n50 g unsalted butter\r\n50 g dark brown sugar\r\n30 g golden syrup\r\n1 lug of brandy\r\n\r\nDirections:\r\nPreheat the oven to 160C/gas 2-3 and place a tray of water in the bottom (this creates steam during cooking to keep the pudding moist). Butter and flour a 25cm bundt tin and set aside. \r\n\r\nRoughly chop the pitted dates, and zest the orange.\r\n\r\nIn a pan, bring 400ml of water to the boil, turn the heat off, add the tea bags and leave to infuse for 3 minutes. \r\n\r\nDiscard the tea bags and bring back to the boil. Whisk in the bicarbonate of soda and add the dates. Leave to one side to cool to room temperature.\r\n\r\nCream the butter and sugar in a food processor or with a hand-held mixer until incorporated, light and fluffy.\r\n\r\nIn a bowl, whisk together the eggs, vanilla, orange zest, golden syrup and treacle until just combined. Pour into the butter mixture in stages, beating until smooth each time.\r\n\r\nSieve the flour and fold gently into the batter. Stir in the date mixture, then pour into the bundt tin.\r\n\r\nBake for 1 hour, or until a knife inserted into the centre comes out clean. Leave to cool in the tins for 5 minutes. \r\n\r\nPop all the sauce ingredients in a small pan and bring to the boil over a medium heat. Simmer for 5 minutes, stirring, then remove from the heat.\r\n\r\nInvert the pudding onto a platter and baste with a little of the sauce. Delicious served with clotted cream and the remaining sauce on the side.', 'Dessert', NULL),
(9, 'Chocolate and Caramel Tart with Hazelnuts', 'Serves 12\r\n\r\nIngredients:\r\nIngredients:\r\nPASTRY\r\n150 g plain flour\r\n40 g cocoa\r\n25 g icing sugar\r\n120 g unsalted butter\r\n1 large free-range egg yolk\r\n\r\nFILLING\r\n300 g caster sugar\r\n60 ml runny honey\r\n300 ml double cream\r\n45 g unsalted butter\r\n\r\nNUT BRITTLE\r\n50 g blanched hazelnuts\r\n100 g caster sugar\r\nSPUN SUGAR\r\n100 g caster sugar\r\n1/2 tablespoon glucose syrup\r\n\r\nDirections:\r\nTo make the pastry, sift the flour, cocoa and icing sugar into a bowl. Rub in the butter until it resembles fine breadcrumbs and stir in a pinch of sea salt. \r\n\r\nStir in the egg yolk and 45ml water, then bring together into a dough, add more flour if necessary. Wrap in clingfilm and leave to rest in the fridge for 30 minutes. \r\n\r\nPreheat the oven to 190ºC/375ºF/gas 5.\r\n\r\nLine a 25cm fluted tart tin with the pastry, pressing into the sides. Line with baking paper and fill with baking beads or rice. Set on a baking sheet and rest in the fridge for 30 minutes.\r\n\r\nTransfer the tin to the oven on the baking sheet for 15 minutes. Roast the nuts for the brittle at the same time, until they are a golden brown. Remove the baking paper and beads or rice. Return the pastry to the oven for 5 minutes, or until the base has cooked through. Set aside. \r\n\r\nTo make the filling, place the sugar and honey into a pan and set over a medium heat. Once the sugar begins to melt, swirl in circular motions to start the caramel until it’s golden. Remove from the heat, whisk in the cream and butter. Place back onto the heat and whisk until the caramel turns golden brown. Remove and pour into the tart case, then set aside.\r\n\r\nTo make the nut brittle, line a baking tray with baking paper. Put the sugar in a pan with the thermometer and set over a medium heat. Have your roasted hazelnuts ready. \r\n\r\nWhen the sugar starts to gently bubble at the edges, tilt the pan to ensure the sugar melts evenly. Before long (about 3 minutes) all of it will be melted. When it gets to 150ºC, remove the pan from the heat and swirl in the hazelnuts (do not stir with a spoon). Quickly pour it onto the tray and leave to aside. \r\n\r\nWhen the nut brittle is cool, chop or blitz in a processor to small pieces. Sprinkle the pieces of brittle over the top of the finished tart.\r\n\r\nTo decorate with spun sugar, heat the sugar and glucose with 50ml water in the same pan, over a medium heat. Allow the sugar to melt, stir for even texture. Heat for 8 to 10 minutes, until 150ºC, then remove and quickly place into an ice bath to stop the process and set aside.\r\n\r\nTake two forks and pull the sugar out, twirling the forks to make long strands into nests. Alternatively, place a piece of baking paper on your countertop. While holding a long metal spoon with one hand over the paper, pull the sugar from the pan and quickly flick it back and forth over the long spoon, allowing the sugar to form long thin strands, which you can gather. Use these to decorate the top of the tart. \r\n\r\nDelicious served with crème fraîche or yoghurt.', 'Dessert', NULL),
(10, 'Anzac Biscuits', 'Serves 16\r\n\r\nIngredients:\r\n100 g unsalted butter\r\n2 tablespoons golden syrup\r\n1 teaspoon bicarbonate of soda\r\n120 g plain flour\r\n80 g porridge oats\r\n100 g golden caster sugar\r\n80 g desiccated coconut\r\n1 teaspoon vanilla extract\r\n1 orange , zest from\r\n\r\nDirections:\r\nPreheat the oven to 180ºC/350ºF/gas 4. Line 2 large baking trays with greaseproof paper.\r\n\r\nMelt the butter in a small pan over a low heat, then stir in the golden syrup. In a small bowl, combine the bicarbonate of soda with 3 tablespoons of boiling water, then stir it into the butter mixture.\r\n\r\nCombine the flour, oats, sugar and coconut in a medium bowl. Make a well in the middle, then add the butter mixture, vanilla extract and orange zest. Give the wet ingredients a good mix, then gradually stir in the dry ingredients to combine.\r\n\r\nPlace heaped tablespoons of the mixture onto the prepared baking trays, leaving a rough 3cm gap between each one. Place in the hot oven for around 10 minutes, or until golden, then transfer to a wire cooling rack to cool completely. ', 'Baking', NULL),
(11, 'Everything Cookies', 'Serves 18\r\n\r\nIngredients:\r\n200 g unsalted butter , (at room temperature)\r\n150 g golden caster sugar\r\n50 g light brown muscovado sugar\r\n1 large free-range egg\r\n1 teaspoon vanilla extract\r\n325 g self-raising flour\r\n100 g dark, milk or white chocolate , (or a mixture)\r\n50 g toffees\r\n50 g dried fruit , such as apricots, cranberries, cherries\r\n50 g shelled nuts , such as Brazils, almonds, pecans and walnuts\r\n\r\nDirections:\r\nPreheat the oven to 180ºC/gas 4. In a large bowl, beat the butter and both sugars together for 2 minutes, until light and fluffy. Beat in the egg and vanilla extract until combined. \r\n\r\nAdd the flour and 1 teaspoon of sea salt to the cookie mixture and mix well to form a dough. Roughly chop the chocolate and toffees, then stir through the mixture with the dried fruit and nuts, or whatever you are throwing in, then cover and leave in the fridge to firm up for at least 2 hours (or up to 3 days, if you want to make these in advance). Chill as long as you can, as the flavours will deepen over time.\r\n\r\nLine a large baking sheet with non-stick baking paper and use an ice cream scoop to form balls of cookie dough. Space the scoops out well – 6 in a batch is plenty as they will spread – and bake for about 15 minutes, until pale golden. \r\n\r\nLeave the cookies on the tray for a few minutes to firm up, then transfer to wire racks to cool while you bake the next batch.', 'Baking', NULL),
(12, 'Veggie Noodle Stir Fry\r\n', 'Serves 4\r\n\r\nIngredients:\r\n250 g thin free-range egg noodles\r\n2 limes\r\n3 tablespoons plum sauce\r\n1 tablespoon runny honey\r\nlow-salt soy sauce\r\n6 spring onions\r\n2 cloves of garlic\r\n3cm piece of ginger\r\n100 g sugar snap peas\r\n1 red pepper\r\n1 yellow pepper\r\n¼ of a Chinese cabbage , (180g)\r\n100 g beansprouts\r\n100 g frozen peas\r\n2 tablespoons vegetable oil\r\nsesame oil , optional\r\n\r\nDirections:\r\nCook the noodles according to packet instructions, then drain and refresh under cold running water and drain', 'Vegetarian', NULL),
(13, 'Vegetarian Lasagne', 'Serves 8\r\n\r\nIngredients:\r\n3 aubergines\r\n3 cloves garlic , peeled and sliced\r\na few sprigs fresh thyme , leaves picked\r\n1 dried red chilli , crumbled\r\n6 tablespoons extra virgin olive oil\r\n2x400 g organic tinned plum tomatoes\r\n1 splash balsamic vinegar\r\n1 bunch fresh basil , leaves picked and stalks chopped\r\n150 g Cheddar cheese , grated\r\n2 handfuls Parmesan cheese , grated\r\n6-8 fresh lasagne sheets\r\n\r\nDirections:\r\nPreheat the oven to 200°C/400ºF/gas 6. Steam the whole aubergines over a pan of si', 'Vegetarian', NULL),
(14, 'Veggie Enchiladas', 'Serves 4\r\n\r\nIngredients:\r\n2 red or yellow peppers\r\n2 corn on the cob\r\nolive oil\r\n1 x 400 tin g black beans\r\n½ teaspoon ground cumin\r\n6 spring onions\r\n1 fresh red chilli\r\n1 bunch fresh coriander\r\n1 lime\r\n2 cloves garlic\r\n1 x 400 tin g plum tomatoes\r\n8 small corn tortillas\r\n40 g feta cheese\r\n\r\nDirections:\r\nPreheat the oven to 200ºC/400ºF/gas 6. Place a griddle pan over a high heat to get smoking hot. Halve and deseed the peppers, then add to the griddle with the corn. Grill for 8 to 10 minutes, or', 'Vegetarian', NULL),
(15, 'Squash and Ricotta Pasta Bake', 'Serves 4\r\n\r\nIngredients:\r\n1 butternut squash , peeled, deseeded and chopped into 2.5cm pieces\r\nolive oil\r\n2 cloves of garlic , peeled and finely sliced\r\n1 bunch of fresh basil , leaves picked, stalks finely chopped\r\n1 x 400 g tins of chopped tomatoes\r\nsea salt\r\n500 g dried penne\r\nfreshly ground black pepper\r\n3 tablespoons ricotta cheese\r\n750 ml organic vegetable stock\r\n150 g mozzarella ball\r\n1 handful Parmesan cheese , freshly grated\r\n2 sprigs fresh sage , leaves picked\r\n\r\nDirections:\r\nPreheat your oven to 200°C/400°F/gas 6. Place the squash on a baking tray, drizzle with olive oil and pop in the hot oven for around 15 minutes, or until tender.\r\n\r\nPour a couple of lugs of olive oil into a large frying pan, add the garlic and basil stalks and fry for a couple of minutes. Add your tomatoes to the pan, breaking them up with a wooden spoon and bring to the boil. Drop in the roasted squash, bring to the boil, then simmer for 10 minutes. \r\n\r\nMeanwhile, bring a large pot of salted water to the boil, add the penne and cook for a couple of minutes less than it says on the packet. Drain, then toss with the sauce. \r\n\r\nTear up the basil leaves and sprinkle into the pan with some salt and pepper. Stir in the ricotta and the stock, then bring back to the boil. \r\n\r\nRub a large baking tray, ovenproof pan or earthenware dish with olive oil and spoon in all the pasta and sauce. Tear over the ball of mozzarella and top with the Parmesan. Rub the sage leaves with a little olive oil and put on top. \r\n\r\nPop it into the preheated oven and bake for 15 minutes or until golden and bubbling. Serve with a crisp green salad. ', 'Pasta', NULL),
(16, 'Meatballs and Pasta', 'Serves 4\r\n\r\nIngredients:\r\n4 sprigs fresh rosemary\r\n12 Jacob''s cream crackers\r\n2 heaped teaspoons Dijon mustard\r\n500 g quality minced beef, higher-welfare pork, or a mixture of the two\r\n1 heaped tablespoon dried oregano\r\n1 large free-range egg\r\nsea salt\r\nfreshly ground black pepper\r\nolive oil\r\n1 bunch fresh basil\r\n1 medium onion\r\n2 cloves garlic\r\n½ fresh or dried red chilli\r\n2x400 g tinned chopped tomatoes\r\n2 tablespoons balsamic vinegar\r\n400 g dried spaghetti or penne\r\nParmesan cheese , for grating\r\n\r\nDirections:\r\nPick the rosemary leaves off the woody stalks and finely chop them. Wrap the crackers in a tea towel and smash up until fine, breaking up any big bits with your hands. Add to a mixing bowl with the mustard, minced meat, chopped rosemary and oregano. Crack in the egg and add a good pinch of salt and pepper.\r\n\r\nWith clean hands scrunch and mix up well. Divide into 4 large balls. With wet hands, divide each ball into 6 and roll into little meatballs – you should end up with 24. Drizzle them with olive oil and jiggle them about so they all get coated. Put them on a plate, cover and place in the fridge until needed.\r\n\r\nPick the basil leaves, keeping any smaller ones to one side for later. Peel and finely chop the onion and the garlic. Finely slice the chilli. Put a large pan of salted water on to boil. Next, heat a large frying pan on a medium heat and add 2 lugs of olive oil. Add your onion to the frying pan and stir for around 7 minutes or until softened and lightly golden.\r\n\r\nThen add your garlic and chilli, and as soon as they start to get some colour add the large basil leaves. Add the tomatoes and the balsamic vinegar. Bring to the boil and season to taste. Meanwhile, heat another large frying pan and add a lug of olive oil and your meatballs. Stir them around and cook for 8–10 minutes until golden (check they’re cooked by opening one up – there should be no sign of pink).\r\n\r\nAdd the meatballs to the sauce and simmer until the pasta is ready, then remove from the heat. Add the pasta to the boiling water and cook according to the packet instructions. Saving some of the cooking water, drain the pasta in a colander. Return the pasta to the pan. Spoon half the tomato sauce into the pasta, adding a little splash of your reserved water to loosen. Serve on a large platter, or in separate bowls, with the rest of the sauce and meatballs on top. Sprinkle over the small basil leaves and some grated Parmesan.', 'Pasta', NULL),
(17, 'Spring Rolls', 'Serves 8\r\n\r\nIngredients:\r\n40 g dried Asian mushrooms\r\n50 g vermicelli noodles\r\n200 g Chinese cabbage , finely sliced\r\n1 carrot , peeled and julienned\r\n3 spring onions , white parts sliced on the diagonal, green parts finely sliced into ribbons\r\n1 thumb-sized piece of ginger , peeled and grated\r\n1 red chilli , finely chopped\r\n1 large bunch of Thai basil , roughly chopped\r\n1 large bunch of coriander , roughly chopped\r\n20 ml sesame oil\r\n75 g beansprouts\r\n3 tablespoons toasted peanuts , crushed\r\n2 tablespoons reduced-salt soy sauce\r\n2 tablespoons oyster sauce\r\n1 tablespoon cornflour\r\n16 large spring roll wrappers , thawed if frozen\r\n1 tablespoon five spice powder\r\n1 litre groundnut oil\r\nsweet chilli sauce , to serve\r\n\r\nDirections:\r\nPut your mushrooms in a medium-sized bowl, cover with hot water and leave for 10 minutes, or until soft. Meanwhile, place the noodles in a large bowl, cover with boiling water and leave for 1 minute. Drain, rinse under cold water, then set aside. \r\n\r\nFor the filling, put the cabbage, carrot, white part of the spring onion, ginger, chilli and herbs in a large bowl along with the rice noodles. Add the sesame oil, beansprouts, peanuts, soy and oyster sauces, and mix well. When they’re ready, drain the mushrooms, then chop them and stir into the filling. Season to taste.\r\n\r\nIn a small bowl, blend the cornflour and 2 tablespoons of cold water.\r\n\r\nNext, lay one spring-roll wrapper, smooth-side down, on a clean surface as a diamond shape, with one corner pointing down towards you. Sprinkle a little of the five spice powder over it, then place another wrapper on top (the extra thickness will stop the rolls from breaking open while cooking). \r\n\r\nSpoon 2 tablespoons of the filling on the bottom corner of the double wrapper. Brush each corner with the cornflour mixture, then start rolling up from the bottom. When the filling is covered, pull the corners in from each side (to seal the ends as you go). Continue rolling until the filling is tightly covered, then press to seal the top corner.\r\n\r\nLay the finished roll on a large baking tray and cover with a damp tea towel. Continue until you’ve filled all the wrappers. \r\n\r\nHeat the groundnut oil in a large wok or saucepan over a medium heat. To check whether the oil is ready, drop in a piece of potato; it should sizzle and start to turn golden. In small batches, carefully lower the spring rolls into the oil and deep-fry for 2–3 minutes, or until golden brown. Remove with a slotted spoon and drain on kitchen paper.\r\n\r\nServe with the sweet chilli sauce and sliced spring-onion tops.', 'Appetizers', NULL),
(18, 'Cheese Fondue', 'Serves 10\r\n\r\nIngredients:\r\n2 shallots , peeled and finely chopped\r\nolive oil\r\nsea salt\r\nfreshly ground black pepper\r\n2 sprigs of fresh thyme or marjoram , leaves picked\r\n50ml cider\r\n400g Cheddar cheese\r\n400g Gruyere cheese\r\n150g blue cheese\r\n2 tablespoons crème fraîche\r\n\r\nDirections:\r\nHeat a lug of oil in a frying pan on a medium heat. Put a large saucepan with an inch or two of hot water on a low heat. Put a large heatproof bowl over the saucepan so you have a bain-marie. \r\n\r\nAdd the chopped shallots to the frying pan and cook them gently for around 5 to 10 minutes with a pinch of salt and pepper and the herbs. Once they’ve softened, but not coloured, add them to the bowl with the cider and all of the cheese. \r\n\r\nLeave the cheese to slowly melt down, only stirring now and then to help it along. Once it has mostly melted, stir in the crème fraîche then a swig or two of hot water to loosen the mixture a little. Have a taste to check the seasoning, and get all of your dipping ingredients and skewers at the ready. I like homemade croutons, some bread sticks and a few leftover roasted potatoes cut in half for mine, but really anything you like to eat with cheese will be good here. When it’s runny and delicious get everyone around the bowl and watch it disappear! ', 'Appetizers', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recipe_tags`
--

CREATE TABLE `recipe_tags` (
  `recipe_id` mediumint(8) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tags_id` int(11) NOT NULL,
  `tags` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `username`, `password`, `role`) VALUES
(1, 'myrecipesuser@mail.com', 'user01', 'password', 'user'),
(12, 'example01@mail.com', 'example-user', '$2y$10$CmECyPU8TtysbUHjezxyqukxR1DC2UxmeV8D5MHn8UfZnOcHvSNEy', 'user'),
(13, 'example02@mail.com', 'example02', '$2y$10$Jco3PCql3AvZwy5EwlD/ru2CCTooQriVXEYiutqFhLSzMUI6TKjx.', 'user'),
(14, 'admin@mail.com', 'admin', '$2y$10$WIWBZTzs5ON4KZl9S4U/ee2ks9G9mW7Q/ux2wk1ejjs8EJJvXM3b.', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`recipe_id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`recipe_id`);

--
-- Indexes for table `recipe_tags`
--
ALTER TABLE `recipe_tags`
  ADD PRIMARY KEY (`recipe_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tags_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `recipe_id` mediumint(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `recipe_id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tags_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
